'use strict';


const Service = require('egg').Service;
const { workflowConfig } = require('../../config/workflow.js');
const { generalArrange } = require('../util/arrange');
const Promise = require('bluebird');


class WorkflowService extends Service {

  /*
  constructor(ctx) {
    super(ctx);
  }
  */

  async showWorkflow() {

    const { ctx } = this;
    const arrangeContext = {
      step: 0,
    };

    try {
      await generalArrange(arrangeContext, workflowConfig, ctx.service, ctx);
    } catch (err) {
      throw err;
    }

    return 'ok';
  }

  async node1(arrangeContext, params) {
    arrangeContext.step++;
    console.log(arrangeContext);

    const timeout = params.timeout ? params.timeout : 0;
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve();
      }, timeout);
    });
  }

  async node2(arrangeContext, params) {
    arrangeContext.step++;
    console.log(arrangeContext);

    const timeout = params.timeout ? params.timeout : 0;
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve();
      }, timeout);
    });
  }

  async node3(arrangeContext, params) {
    arrangeContext.step++;
    console.log(arrangeContext);

    const timeout = params.timeout ? params.timeout : 0;
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve();
      }, timeout);
    });
  }

  async node4(arrangeContext, params) {
    arrangeContext.step++;
    console.log(arrangeContext);

    const timeout = params.timeout ? params.timeout : 0;
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve();
      }, timeout);
    });
  }

  async node5(arrangeContext, params) {
    arrangeContext.step++;
    console.log(arrangeContext);

    const timeout = params.timeout ? params.timeout : 0;
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve();
      }, timeout);
    });
  }

  async node6(arrangeContext, params) {
    arrangeContext.step++;
    console.log(arrangeContext);

    const timeout = params.timeout ? params.timeout : 0;
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve();
      }, timeout);
    });
  }

  async node7(arrangeContext, params) {
    arrangeContext.step++;
    console.log(arrangeContext);

    const timeout = params.timeout ? params.timeout : 0;
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve();
      }, timeout);
    });
  }

  async node8(arrangeContext, params) {
    arrangeContext.step++;
    console.log(arrangeContext);

    const timeout = params.timeout ? params.timeout : 0;
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve();
      }, timeout);
    });
  }

  async node9(arrangeContext, params) {
    arrangeContext.step++;
    console.log(arrangeContext);

    const timeout = params.timeout ? params.timeout : 0;
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve();
      }, timeout);
    });
  }

  async node10(arrangeContext, params) {
    arrangeContext.step++;
    console.log(arrangeContext);

    const timeout = params.timeout ? params.timeout : 0;
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve();
      }, timeout);
    });
  }

  async node11(arrangeContext, params) {
    arrangeContext.step++;
    console.log(arrangeContext);

    const timeout = params.timeout ? params.timeout : 0;
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve();
      }, timeout);
    });
  }
}


module.exports = WorkflowService;
