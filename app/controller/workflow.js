'use strict';


const Controller = require('egg').Controller;


class WorkflowController extends Controller {

  async showWorkflow() {

    const { ctx } = this;
    let data;
    try {
      data = await ctx.service.workflow.showWorkflow();

      ctx.body = data;
    } catch (err) {
      ctx.body = 'wrong';
    }
  }
}


module.exports = WorkflowController;
