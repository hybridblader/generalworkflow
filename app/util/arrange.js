'use strict';


const Promise = require('bluebird');


async function generalArrange(arrangeContext, rootTask, service, ctx) {

  return await run(rootTask, ctx);

  async function run(task, ctx) {

    const curType = task.type;

    const { throwError, isAsync } = task;

    if (curType === 'flow') {

      const curSequence = task.sequence;
      const curTasks = task.tasks;

      if (curSequence === 'each') { // 串行
        if (isAsync) {
          return new Promise((resolve, reject) => {
            Promise.each(curTasks, async function(curTask) {
              const { type, name, desc } = curTask;

              arrangeContext.curStepName = name;
              arrangeContext.curStepDesc = desc;

              if (type === 'node') {
                const curFun = await run(curTask, ctx);
                try {
                  await curFun();
                } catch (err) {
                  throw err;
                }
              } else if (type === 'flow') {
                await run(curTask, ctx);
              }
            })
              .then(() => {}, () => {});
            resolve();
          });
        } else if (!isAsync) {
          return new Promise((resolve, reject) => {
            Promise.each(curTasks, async function(curTask) {
              const { type, name, desc } = curTask;

              arrangeContext.curStepName = name;
              arrangeContext.curStepDesc = desc;

              if (type === 'node') {
                const curFun = await run(curTask, ctx);
                try {
                  await curFun();
                } catch (err) {
                  throw err;
                }
              } else if (type === 'flow') {
                await run(curTask, ctx);
              }
            })
              .then(res => {
                resolve(res);
              }, err => {
                if (throwError) {
                  reject(err);
                } else {
                  resolve(err);
                }
              });
          });
        }
      } else if (curSequence === 'parallel') {
        const allTasks = [];
        for (let i = 0; i < curTasks.length; i++) {
          if (curTasks[i].type === 'node') {
            const curFun = await run(curTasks[i], ctx);
            const curPromise = curFun();
            allTasks.push(curPromise);
          } else {
            const curPromise = await run(curTasks[i], ctx);
            allTasks.push(curPromise);
          }
        }

        if (isAsync) {
          return new Promise((resolve, reject) => {
            Promise.all(allTasks)
              .then(() => {}, () => {});
            resolve();
          });
        } else if (!isAsync) {
          return new Promise((resolve, reject) => {
            Promise.all(allTasks)
              .then(res => {
                resolve(res);
              }, err => {
                if (throwError) {
                  reject(err);
                } else {
                  resolve(err);
                }
              });
          });
        }
      }
    } else if (curType === 'node') {

      const curService = task.service;
      const curFunction = task.function;

      const { params, throwError, isAsync, name, desc } = task;

      arrangeContext.curStepName = name;
      arrangeContext.curStepDesc = desc;

      const curFun = service[curService][curFunction];

      const buildPromise = function buildPromise() {
        if (isAsync) { // todo: 不应该出现异步抛出Error, 所以不对异步做throwError判断
          curFun(arrangeContext, params)
            .then(() => {}, () => {});
        } else {
          return new Promise((resolve, reject) => {
            if (throwError) {
              curFun(arrangeContext, params)
                .then(() => resolve(), err => reject(err));
            } else if (!throwError) {
              curFun(arrangeContext, params)
                .then(() => { resolve(); }, () => {
                  resolve();
                });
            }
          });
        }
      };

      return buildPromise;
    }
  }
}


module.exports = {
  generalArrange,
};
